# inventory
bg = 'assets/textures/bg.png'
grid_texture = 'assets/textures/grid.png'

# blocks 2d
grass = 'assets/textures/grass.png'
brick = 'assets/textures/brick.png'
stone = 'assets/textures/stone.png'
dirt = 'assets/textures/dirt.png'

# hand
hand_model = 'assets/models/arm'
arm = 'assets/textures/arm_texture.png'


