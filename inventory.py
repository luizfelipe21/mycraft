from ursina import *
from constantes import *
from random import choice

class Itens_on_hand():
    def __init__(self):
        self.item = []

class BG(Entity):
    def __init__(self):
        super().__init__(
            parent = camera.ui,
            model = 'quad',
            scale = (0.56, 0.86),
            texture = bg
        )

class Item(Draggable):
    def __init__(self, container, draw, pos):
        super().__init__(
            parent = container,
            model = 'quad',
            color = color.white,
            texture = draw,
            origin = (-0.5 * 1.2, 0.5 * 1.2),
            scale = (1 / (container.texture_scale[0] * 1.2), 1 / (container.texture_scale[1] * 1.2)),
            position = pos
        )
        self.texture = draw
    
    def drag(self):
        self.start = self.position
    
    def drop(self):
        if -0.1 > self.x or self.x > 0.95  or self.y > 0.1 or self.y < -0.95:
            self.position = self.start
        else:
            self.x = int((self.x + (self.scale_x/2)) * 5) / 5
            self.y = int((self.y - (self.scale_y/2)) * 8) / 8
            self.overlap()
            mds = (round(self.start[0], 2), round(self.start[1], 2))
            for i in range(len(itens.item)):
                if mds == itens.item[i][1]:
                    itens.item[i][1] = [round(self.position[0], 2), round(self.position[1], 3)]
            print('KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK', itens.item)
        
    def overlap(self):
        for child in self.parent.children:
            if child.x == self.x and child.y == self.y:
                if child == self:
                    continue
                else:
                    child.x = self.start[0]
                    child.y = self.start[1]
    
    def get_cell_pos(self):
        x = self.position[0] * self.parent.texture_scale[0]
        y = self.position[1] * self.parent.texture_scale[1]
        return Vec2(int(x), int(y))


class Grid(Entity):
    def __init__(self):
        super().__init__(
            parent = camera.ui,
            model = 'quad',
            texture_scale = (5, 8),
            scale = (0.5, 0.8),
            texture = grid_texture,
            origin = (-0.5, 0.5),
            position = (-0.25, 0.4)
        )
        self.textures = [grass, stone, dirt, brick]
        for i in self.textures:
            self.add_new_item(i)
        print(itens.item)

    def add_new_item(self, text):
        pos = self.find_free_pos()
        if pos:
            Item(self, text, (pos[0]/5, pos[1]/8))
            itens.item.append([text, (pos[0]/5, pos[1]/8)])
    
    def find_free_pos(self):
        all_positions = [Vec2(x, y) for y in range(0, -8, -1) for x in range(0, 5)]
        filled_pos = [child.get_cell_pos() for child in self.children]
        if len(all_positions) <= len(filled_pos):
            return False
        for cell in all_positions:
            if cell not in filled_pos:
                return cell


itens = Itens_on_hand()
if __name__ == '__main__':
    app = Ursina()
    bg = BG()
    grid = Grid()
    window.exit_button.visible = False
    window.fps_counter.enabled = False
    app.run()