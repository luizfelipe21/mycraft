from ursina import *
from ursina.prefabs.first_person_controller import FirstPersonController
from random import choice, randint
from constantes import *
from inventory import *
from perlin_noise import PerlinNoise
from numpy import floor

app = Ursina()
punch_sound = Audio('assets/punch_sound',loop = False, autoplay = False)
inv = False
fast = False
window.show_ursina_splash = False
scene.fog_color = color.white
scene.fog_density = 0.05

def input(key):
    global inv, block_texture, fast
    if key == 'escape': exit()
    if key == 'e' and not inv: 
        inv = True
        bg.enable()
        grid.enable()
        player.disable()
    elif key == 'e' and True:
        inv = False
        bg.disable()
        grid.disable()
        player.enable()
    if held_keys['shift'] and not fast: 
        player.speed = player.speed * 3
        fast = True
    elif not held_keys['shift'] and fast:
        player.speed = player.speed / 3
        fast = False


class MiniGrid(Entity):
    def __init__(self):
        super().__init__(
            parent = camera.ui,
            model = 'quad',
            texture_scale = (5, 8),
            scale = (0.5, 0.8),
            texture = grid_texture,
            origin = (-0.5, 0.5),
            position = (-0.25, 0.4)
        )
 

class Hand(Entity):
    def __init__(self):
        super().__init__(
            parent = camera.ui,
            model = hand_model,
            texture = arm,
            scale = 0.2,
            position = Vec2(0.4, -0.6),
            rotation = Vec3(150, -10, 10),
        )
    
    def input(self, key):
        if key == 'left mouse down' or key == 'right mouse down':
            invoke(self.active)
            punch_sound.play()
            invoke(self.passive, delay=.1)
    
    def active(self):
        self.position = Vec2(0.3, -0.5)
        self.rotation = Vec3(170, -20, 10)
    
    def passive(self):
        self.position = Vec2(0.4, -0.6)
        self.rotation = Vec3(150, -10, 10)

# def update():
#     print(itens.item)


chunk = Entity(model = None, texture = None, color = None)
if __name__ == '__main__':
    noise = PerlinNoise(octaves=1, seed = 1)
    tam = 30
    freq = 12
    amp = 1
    # construindo o mundo
    for i in range(tam * tam):
        voxel = Entity(model = 'cube', texture = 'white_cube', color = color.green)
        voxel.x = floor(i/tam)
        voxel.z = floor(i%tam)
        voxel.y = floor(noise([voxel.x / freq, voxel.z / freq]) * amp) 
        voxel.parent = chunk
    chunk.combine()
    chunk.collider = 'mesh'
                    
    sky = Sky()                                    # cria o ceu
    player = FirstPersonController()                                    # cria o player
    player.x = player.z = tam/2
    player.y = 12
    hand = Hand()                                                       # cria a mao
    bg = BG() 
    grid = Grid()
    print(itens)
    bg.disable()
    grid.disable()

    app.run()

# class JogarButton(Button):
#     def __init__(self):
#         super().__init__(
#             text = 'JOGAR',
#             color = color.green,
#             scale = (0.2, 0.1)
#         )
    
#     def input(self, key):
#         if key == 'left mouse down':
#             # criando o mapa
#             for i in range(-10, 10):
#                 for k in range(-10, 10):
#                     voxel = Voxel((i,0,k))
#                     voxel.collider = 'box'
#             sky = Sky()
#             player = FirstPersonController()
#             hand = Hand()
#             destroy(menu.jogar)
#             destroy(menu.sair)
#             destroy(menu)

# class Menu(Entity):
#     def __init__(self):
#         super().__init__(
#             model = 'cube', 
#             texture = menu,
#             scale = (16, 9)
#         )
#         self.jogar = JogarButton()
#         self.sair = Button(text='SAIR', color=color.red, scale=(.2, 0.1), origin_y = 2)
#         self.sair.on_click = application.quit